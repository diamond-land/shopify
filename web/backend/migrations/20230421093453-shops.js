'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('shops', {
      id: {
        type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false,
      },
      shopId: {
        type: Sequelize.STRING(50), allowNull: false,
      },
      name: {
        type: Sequelize.STRING(255), allowNull: false,
      },
      state: {
        type: Sequelize.STRING(255), allowNull: false,
      },
      scopes: {
        type: Sequelize.TEXT, allowNull: false,
      },
      accessToken: {
        type: Sequelize.STRING(255), allowNull: false,
      },
      createdAt: {
        type: Sequelize.DATE,
      },
      updatedAt: {
        type: Sequelize.DATE,
      },
      deletedAt: {
        type: Sequelize.DATE,
      },
    }, {
      charset: 'utf8mb4',
    });
  },

  down: async (queryInterface) => {
    return queryInterface.dropTable('shops');
  },
};
