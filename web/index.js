// @ts-check
import { join } from "path";
import { readFileSync } from "fs";
import express from "express";
import serveStatic from "serve-static";

import shopify from "./shopify.js";
import productCreator from "./product-creator.js";
import GDPRWebhookHandlers from "./gdpr.js";

const PORT = parseInt(process.env.BACKEND_PORT || process.env.PORT, 10);

const STATIC_PATH =
  process.env.NODE_ENV === "production"
    ? `${process.cwd()}/frontend/dist`
    : `${process.cwd()}/frontend/`;

const app = express();

// Set up Shopify authentication and webhook handling
app.get(shopify.config.auth.path, shopify.auth.begin());
app.get(
  shopify.config.auth.callbackPath,
  shopify.auth.callback(),
  shopify.redirectToShopifyOrAppRoot()
);
app.post(
  shopify.config.webhooks.path,
  shopify.processWebhooks({ webhookHandlers: GDPRWebhookHandlers })
);

// If you are adding routes outside of the /api path, remember to
// also add a proxy rule for them in web/frontend/vite.config.js

app.use("/api/*", shopify.validateAuthenticatedSession());

app.use(express.json());

app.get("/api/products", async (_req, res) => {
  console.log("---------Get Products---------");
  const countData = await shopify.api.rest.Product.all({
    session: res.locals.shopify.session,
  });
  res.status(200).send(countData);
});

app.get("/api/shops", async (_req, res) => {
  console.log("---------Get Shops---------");
  const countData = await shopify.api.rest.Shop.all({
    session: res.locals.shopify.session,
  });
  res.status(200).send(countData);
});

app.get("/api/products/update", async (_req, res) => {
  console.log("---------Get single product---------");
  const product = await shopify.api.rest.Product.find({
    session: res.locals.shopify.session,
    id: 8262153306412
  });
  if (!product) {
    res.status(404).send({ success: false, error: "Product not found" });
  }
  const productUpdate = new shopify.api.rest.Product({session: res.locals.shopify.session});
  productUpdate.id = 8262153306412;
  productUpdate.tags = "carbon import";
  productUpdate.save({
    update: true,
  });
  res.status(200).send(product);
});

app.get("/api/orders", async (_req, res) => {
  console.log("---------Get orders---------");
  const orders = await shopify.api.rest.Order.all({
    session: res.locals.shopify.session,
  });
  res.status(200).send(orders);
});

app.get("/api/orders/count", async (_req, res) => {
  console.log("---------Get count order---------");
  const orders = await shopify.api.rest.Order.count({
    session: res.locals.shopify.session,
  });
  res.status(200).send(orders);
});

app.get("/api/orders/single", async (_req, res) => {
  console.log("---------Get singgle order---------");
  const orders = await shopify.api.rest.Order.count({
    session: res.locals.shopify.session,
  });
  res.status(200).send(orders);
});

app.get("/api/products/count", async (_req, res) => {
  console.log("Counting products");
  const countData = await shopify.api.rest.Product.count({
    session: res.locals.shopify.session,
  });
  res.status(200).send(countData);
});


app.get("/api/products/create", async (_req, res) => {
  let status = 200;
  let error = null;

  try {
    await productCreator(res.locals.shopify.session);
  } catch (e) {
    console.log(`Failed to process products/create: ${e.message}`);
    status = 500;
    error = e.message;
  }
  res.status(status).send({ success: status === 200, error });
});

app.use(shopify.cspHeaders());
app.use(serveStatic(STATIC_PATH, { index: false }));

app.use("/*", shopify.ensureInstalledOnShop(), async (_req, res, _next) => {
  return res
    .status(200)
    .set("Content-Type", "text/html")
    .send(readFileSync(join(STATIC_PATH, "index.html")));
});

app.listen(PORT);
